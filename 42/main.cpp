#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <unordered_set>
using namespace std;

int main()
{
    //26 is the highest possible letter, 15 is an upper bound on the word length (from eyeballing)
    //26 * 15 = 390, some algebra shows that t_28 is first num greater than 390
    unordered_set<int> triangle_nums;
    for (int i = 1; i <= 28; ++i)
    {
        triangle_nums.insert((int)(0.5 * i * (i + 1)));
    }

    ifstream f;
    f.open("words.txt");
    string temp;
    int count = 0;
    while (f >> temp)
    {
        int result = 0;
        for (uint i = 0; i < temp.size(); ++i)
        {
            result += (temp[i] - 'A' + 1);
        }
        if (triangle_nums.count(result) != 0)
        {
            ++count;
        }
    }
    cout << "Total: " << count << endl;
    return EXIT_SUCCESS;
}