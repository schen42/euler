#include <iostream>
#include <string>
#include <math.h>
using namespace std;
int main()
{
    double sum = 0;
    //999,999  >9^5 + 9^5 + 9^5 + 9^5 + 9^5 + 9^5 is 6 digits long, so 999,999 is a decent upper bound
    for (int i = 4000; i <= 999999; ++i)
    {
        double curr_sum = 0;
        string temp = to_string(i);
        for (unsigned int j = 0; j < temp.size(); ++j)
        {
            curr_sum += pow(temp[j] - '0', 5);
        }
        if (curr_sum == i)
        {
            sum += curr_sum;
        }
    }
    cout << "sum=" << sum << endl;
    return 0;
}