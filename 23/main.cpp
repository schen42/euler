#include <iostream>
#include <vector>
#include <cstdio>
#include <math.h>
#define UPPER_LIMIT 28123

//Measured run-time ~600ms

int main()
{
    //find the abundant numbers under 28123...any higher and we know it can be written as 
    //the sum of 2 abundant numbers
    //NOTE: if 12 is the smallest abundant number, then we really only have to search 28123 - 12
    //since 12 + any number greater than 28123 - 12 is known to not satisfy the problem
    std::vector<int> abundant;
    int r = 0; //remainder
    for (int i = 1; i <= UPPER_LIMIT - 12; ++i) //i is a possible non abundant numbers
    {
        int sum = 0;
        int step = 1 + (i % 2); //if odd, only consider odd factors
        for (int j = 1; j * j <= i; j += step) //j is a possible divisor
        {
            if ((r = (i % j)) == 0)
            {
                sum += j;
                if (j != (i / j)) sum += i / j; //don't push back the same divisor twice (i.e. square numbers)
            }
        }
        sum -= i;
        if (sum > i)
        {
            abundant.push_back(i);
        }
    }

    //Using the abundant numbers, we can pairwise add to reduce the solution space
    //Add each sum to a hash table...this represents the values that CAN be represented as 
    //the sum of two abundant numbers...*2 is approximate
    //NOTE: The C++ hash table(unordered_set) is much slower because of collisions
    bool badvalues[UPPER_LIMIT * 2] = { false };
    for (unsigned int i = 0; i < abundant.size(); ++i)
    {
        for (unsigned int j = 0; j < abundant.size(); ++j)
        {
            badvalues[abundant[i] + abundant[j]] = true;
        }
    }

    double sum = 0;
    for (int i = 1; i <= UPPER_LIMIT; ++i) //i is a possible non abundant numbers
    {
        if (!badvalues[i])
            sum += i;
    }

    printf("Answer = %f\n", sum);

    return 0;
}