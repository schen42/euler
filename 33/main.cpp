#include <iostream>
#include <string>
#include <math.h>
using namespace std;

int main()
{
    int complete_numerator, complete_denominator;
    double fraction; //may need to use arbitrary library
    int final_frac_num = 1;
    int final_frac_denom = 1;
    for (int num1 = 1; num1 <= 9; ++num1)
    {
        for (int num2 = 1; num2 <= 9; ++num2)
        {
            for (int denom1 = 1; denom1 <= 9; ++denom1)
            {
                for (int denom2 = 1; denom2 <= 9; ++denom2)
                {
                    complete_numerator = num1 * 10 + num2;
                    complete_denominator = denom1 * 10 + denom2;
                    fraction = (double)complete_numerator / complete_denominator;
                    if (fraction >= 1) continue; //only consider values < 1
                    if (num1 == denom2)
                    {
                        if ((double)num2 / denom1 == fraction)
                        {
                            cout << num1 << num2 << "/" << denom1 << denom2 << "=" << num2 << "/" << denom1 << endl;
                            final_frac_num *= num2;
                            final_frac_denom *= denom1;
                        }
                    }
                    else if (num2 == denom1)
                    {
                        if ((double)num1 / denom2 == fraction)
                        {
                            cout << num1 << num2 << "/" << denom1 << denom2 << "=" << num1 << "/" << denom2 << endl;
                            final_frac_num *= num1;
                            final_frac_denom *= denom2;
                        }
                    }
                }
            }
        }
    }
    cout << "The final non-simplified fraction is: " << final_frac_num << "/" << final_frac_denom << endl;
    return 0;   
}
