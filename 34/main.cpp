#include <iostream>
#include <math.h>
using namespace std;
typedef unsigned long long bigint;
//assume n is [0-9]
bigint compute_factorial(bigint n, bigint memoized[])
{
    if (memoized[n] == 0)
    {
        bigint result = 1;
        for (unsigned int i = 2; i <= n; ++i)
        {
            result *= i;
        }
        return result;
    }
    else
    {
        return memoized[n];
    }
}

int main()
{
    int total = 0;
    bigint factorials[10] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    //9 -> 9! = 362880, 99 -> 2*9! = 725760, 999 -> 1088640, 9999 -> 1451520,
    //99999 -> 1814400, 999999 -> 2177280, 9,999,999 -> 2,540,160
    //9,999,999 is fair upper bound, 1! and 2! not included
    for (bigint i = 3; i < 9999999; ++i)
    {
        string num = to_string(i);
        bigint sum = 0;
        for (unsigned int j = 0; j < num.size(); ++j)
        {
            sum += compute_factorial(num[j] - '0', factorials);
        }
        if (sum == i)
        {
            total += i;
            cout << i << endl;
        }
    }

    cout << "total: " << total << endl;
    return EXIT_SUCCESS;   
}
