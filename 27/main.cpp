#include <iostream>
#include <unordered_map>
using namespace std;

//Run-time approx 150ms

//NOTE: I assumed the primes found were distinct, which worked out just fine, apparently

bool is_prime(int x)
{
    if (x < 0 || x == 0 || x == 1) return false;

    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    return true;
}

int main()
{
    //NOTE: Memoization is actually slower on my machine
    unordered_map<int, bool> known_primes;
    int most_primes = -1;
    int a_with_most_primes, b_with_most_primes;
    //brute force method, hooray
    for (int a = -999; a <= 999; ++a)
    {
        for (int b = -999; b <= 999; ++b)
        {
            int curr_prime_count = 0;
            for (int n = 0; true; ++n)
            {
                if (known_primes.count(n * n + a * n + b))
                {
                    ++curr_prime_count;
                }
                else if (is_prime(n * n + a * n + b))
                {
                    known_primes[n * n + a * n + b] = true;
                    ++curr_prime_count;
                }
                else
                {
                    break;
                }
            }

            if (curr_prime_count > most_primes)
            {
                most_primes = curr_prime_count;
                a_with_most_primes = a;
                b_with_most_primes = b;
            }
        }
    }

    cout << "a=" << a_with_most_primes << ", b=" << b_with_most_primes << " # of primes=" << most_primes << endl;
    cout << "product=" << a_with_most_primes * b_with_most_primes << endl;
    /*for (auto itr = known_primes.begin(); itr != known_primes.end(); itr++)
    {
        cout << itr->first << ", ";
    }
    cout << endl;*/
    return 0;
}