#include <iostream>
#include <string>
#include <unordered_map>
#include <math.h>
using namespace std;

bool is_prime(int x)
{
    static unordered_map<int, bool> primes({{2, true}, {3, true}, {5, true}, {7, true}});
    
    if (x < 0 || x == 0 || x == 1) return false;
    if (primes[x] == true) return true;

    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    primes[x] = true;
    return true;
}

//brute force solution
int main()
{
    int total_found = 0;
    int sum = 0;
    int i = 11;
    while (total_found != 11)
    {
        int n = 0; //the magnitude (10^n)
        int temp = i;
        bool isprime = true;
        while (temp > 0) //right to left (3797, 379, 97, 7)
        {
            if (!is_prime(temp))
            {
                isprime = false;
                break;
            }
            temp /= 10;
            ++n;
        }
        if (isprime) //right to left prime truncatable, now try left to right
        {
            isprime = true;
            temp = i; //left to right (3797, 797, 97, 7)
            for (int j = n - 1; j >= 0; --j)
            {
                if (!is_prime(temp))
                {
                    isprime = false;
                    break;
                }
                temp = temp - (temp / (int)pow(10, j) * (int)pow(10, j));
            }
            if (isprime)
            {
                ++total_found;
                sum += i;
                cout << i << endl;
            }
        }
        ++i;
    }
    cout << "total:" << sum << endl;
    return EXIT_SUCCESS;
}
