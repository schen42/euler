#include <iostream>
#include <string>
#include <unordered_map>
#include <math.h>
bool is_prime(int x)
{
    static unordered_map<int, bool> primes({{2, true}, {3, true}, {5, true}, {7, true}});
    
    if (x < 0 || x == 0 || x == 1) return false;
    if (primes[x] == true) return true;

    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    primes[x] = true;
    return true;
}

bool is_lr_truncatable(int n, int magnitude)
{
    int temp = n;
    for (int i = magnitude; i >= 0; --i)
    {
        if (!is_prime(temp))
            return false;
        temp = temp - (temp / (int)pow(10, i) * (int)pow(10, i));
    }
    return true;
}

//recursively generate right-left truncatable numbers, much faster than brute force (~10 ms)
void generate_truncatable(int n, int level, int &sum)
{
    static int found = 0;
    if (is_prime(n))
    {
        if (is_lr_truncatable(n, level) && n > 10)
        {
            cout << n << endl;
            sum += n;
        }
        for (int i = 1; i <= 9; i += 2)
        {
            generate_truncatable(n * 10 + i, level + 1, sum);
        }
    }
    return;
}

int main()
{
    int sum = 0;
    for (int i = 1; i <= 9; ++i)
        generate_truncatable(i, 0, sum);
    cout << "total: " << sum << endl;
    return EXIT_SUCCESS;
}
