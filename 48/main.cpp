#include <iostream>
#include <stdio.h>
#include <gmp.h>
using namespace std;

int main()
{
    mpz_t sum;
    mpz_init(sum);
    mpz_set_ui(sum, 0);
    mpz_t curr;
    mpz_init(curr);
    for (int i = 1; i <= 1000; ++i)
    {
        mpz_set_ui(curr, i);
        mpz_pow_ui(curr, curr, i);
        mpz_add(sum, sum, curr);
    }
    mpz_out_str (stdout, 10, sum);
    cout << endl;
    return EXIT_SUCCESS;
}