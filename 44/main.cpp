#include <iostream>
#include <unordered_set>
using namespace std;
#define LIMIT 50000

int main()
{
    //generate pentagonal numbers
    uint pentagonal[LIMIT]; 
    unordered_set<uint> pentagonal_nums;
    for (uint i = 0; i < LIMIT; ++i)
    {
        pentagonal[i] = i * (3 * i - 1) / 2; 
        pentagonal_nums.insert(i * (3 * i - 1) / 2);
    }

    for (uint i = 0; i < LIMIT; ++i)
    {
        for (uint j = 1; j < i; ++j)
        {
            uint sum = pentagonal[i] + pentagonal[j];
            uint diff = pentagonal[i] - pentagonal[j];
            if (pentagonal_nums.count(sum) != 0 && pentagonal_nums.count(diff) != 0)
            {
                cout << "D: " << diff << endl;
                return EXIT_SUCCESS; //see below
            }
        }
    }

    return EXIT_FAILURE;
}

/*
    Break Justification

    d(j, k) = f(j) - f(k) = f(z)
    The difference is simply a function of the pentagonal numbers so the first
    difference found, using the algorithm, will be the first pentagonal

    Of course, the initial limit is still arbitrary (the largest uint that will not cause an 
    overflow), but I argue that it's fairly reasonable since even without a limit, we don't know 
    if the solution will fit into any primitive integer data structure.

    10x better performance.
*/