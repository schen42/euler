#include <iostream>
#include <string>
#define PANLEN 10
using namespace std;
typedef unsigned long long lint;

void copy_bit_vec(bool a[], bool b[])
{
    for (int i = 0; i < PANLEN; ++i)
    {
        b[i] = a[i];
    }
}

void init_bit_vec(bool a[])
{
    for (int i = 0; i < PANLEN; ++i)
    {
        a[i] = false;
    }
}

//Re-tooled function from Problem 41
void generate_pandigital(lint root, bool used_digits[], uint level, lint &total)
{
    static uint primes[7] = {2, 3, 5, 7, 11, 13, 17};
    if (level == PANLEN - 1)
    {
        string num = to_string(root);
        bool satisfies_property = true;
        for (int i = 6; i >= 0; --i)
        {
            if (atoi(num.substr(i + 1, 3).c_str()) % primes[i] != 0)
            {
                satisfies_property = false;
                break;
            }
        }
        if (satisfies_property)
            total += root;
        return;
    }

    for (uint i = 0; i < PANLEN; ++i)
    {
        if (!used_digits[i])
        {
            bool temp_used[PANLEN];
            copy_bit_vec(used_digits, temp_used);
            temp_used[i] = true;
            generate_pandigital(root * 10 + i, temp_used, level + 1, total);
        }
    }
}

int main()
{
    lint total = 0;
    bool used_digits[PANLEN];
    for (uint i = 1; i < PANLEN; ++i) //numbers starting with 0 cannot be 0-9 pandigital
    {
        init_bit_vec(used_digits);
        used_digits[i] = true;
        generate_pandigital(i, used_digits, 0, total);
    }
    cout << "total: " << total << endl;
    return EXIT_SUCCESS;
}

//reference 32, 38 for other pandigital problem