#include <iostream>
#include <cstdlib>
#define MAX 9 //maximum digit that will be used (e.g. for 0, 1, 2, MAX = 2)

//Run time ~400 ms

using namespace std;

/* Do a deep copy of arrays */
template <typename T> void copy_a_into_b(const T a[], T b[])
{
    for (int i = 0; i <= MAX; ++i)
    {
        b[i] = a[i];
    }
}

/* Initialize the values of an array */
void init_default(bool x[])
{
    for (int i = 0; i <= MAX; ++i)
    {
        x[i] = false;
    }
}

void init_default(int x[])
{
    for (int i = 0; i <= MAX; ++i)
    {
        x[i] = -1;
    }
}

/*
    @param value: The current generated number (we generate the permutations recursively by appending digits)
    @param root: The current value to append
    @param used: A bit vector representing the digits we have already used (so as to avoid using them again)
    @param count: The number of permutations seen, so we can end the program after we find the millionth permutation
*/
void permute(int root, const bool used[], int pos, const int value[], int &count)
{
    //Keep track of the number
    int value_temp[MAX + 1];
    copy_a_into_b(value, value_temp);
    value_temp[pos] = root;

    //Also keep track of the numbers that have been used
    bool used_temp[MAX + 1];
    copy_a_into_b(used, used_temp);
    used_temp[root] = true;

    //Recurse if we haven't used the digit
    for (int i = 0; i <= MAX; ++i)
    {
        if (!used_temp[i])
        {           
            permute(i, used_temp, pos + 1, value_temp, count);
        }
    }

    if (pos == MAX) //reached the end of a permutation
    {
        ++count;
        if (count == 1000000)
        {
            for (int i = 0; i <= MAX; ++i)
                cout << value_temp[i];
            cout << endl;
            exit(0); //print value and exit
        }
    }
}
    
int main()
{
    int count = 0; //# of permutations found
    for (int i = 0; i <= MAX; ++i)
    {
        bool used[MAX + 1];
        init_default(used);
        int value[MAX + 1];
        init_default(value);
        permute(i, used, 0, value, count);
    }
    return 0;
}