#include <iostream>
#include <cstdlib>
#include <string>
#include <cmath>
#include <unordered_map>
#define LIMIT 1000

using namespace std;
    
//dividend / divisor -->         |-----------
//                       divisor | dividend
int cycle_length(int dividend, int divisor)
{
    //initialize bit vector seeing if remainder repeats (and storing the index in which it first occurred)
    unordered_map<int, int> index_remainder_was_seen;

    //division algorithm, assuming the numerator is 1, and the denominator is greater
    int remaind = 1;
    for (unsigned int i = 0; true; ++i)
    {
        remaind = remaind % divisor;
        if (remaind == 0) //terminated
        {
            return -1; //just return some garbage value since there is no cycle
        }
        if (index_remainder_was_seen.count(remaind) > 0) //cycle detected
        {
            return i - index_remainder_was_seen[remaind];
        }
        index_remainder_was_seen[remaind] = i; 
        remaind *= 10; //basically, since we are always dividing by 1, we can just "pull down" the zero every time    
    }
    return 0;
}

int main()
{
    int longest_cycle = 0;
    int num_with_longest_cycle = 0;
    int l;
    for (int i = 900; i < LIMIT; ++i)
    {
        l = cycle_length(1, i);
        if (l > longest_cycle)
        {
            longest_cycle = l;
            num_with_longest_cycle = i;
        }
    }
    cout << num_with_longest_cycle << " has the longest cycle of length " << longest_cycle << endl;
    return 0;
}