#include <iostream>
#include <string>
#include <math.h>
#include <unordered_map>
using namespace std;

void reset_bit_vec(bool v[10])
{
    v[0] = true; //disqualify 0, since the question only asks for digits 1-9
    for (int i = 1; i <= 9; ++i)
    {
        v[i] = false;
    }
}

/*
    For a number represented as a string, indicate that the number includes
    a digit by setting the bit vector.

    Also, return the status of the number (whether it's POTENTIALLY pandigital or not)
    If there's a 0 digit, it is not 1-9 pandigital
    If there are duplicates digits (i.e. the digit was previously set in the vector), it is not pandigital
*/
bool set_bit_vec(const string &s, bool v[10])
{
    for (unsigned int i = 0; i < s.size(); ++i)
    {
        if (v[s[i] - '0'] == true) //already exists in bit vector 
            return false;
        v[s[i] - '0'] = true;
    }
    return true;
}

int main()
{
    unsigned int prod;
    bool digits_seen[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    unsigned int total = 0;

    unordered_map<unsigned int, bool> seen_prods;

    //Loop through a number from a=[2,9999] and then try every number from [2,a]
    //--0 and 1 cannot be part of a pandigital product, ever
    //--You also can't go past 4 digits because then 4 digits * 1 digit = 4 digits minimum and 4 + 1 + 4 = 9
    //--The last restriction exists for ordering purposes...that way, duplicate products will not occur
    //(123 * 456 and 456 * 123)
    for (unsigned int a = 2; a <= 9999; ++a)
    {
        for (unsigned int b = 2; b < a; ++b)
        {
            reset_bit_vec(digits_seen);
            prod = a * b;
            string temp_a = to_string(a);
            string temp_b = to_string(b);
            string temp_prod = to_string(prod);
            if (temp_a.size() + temp_b.size() + temp_prod.size() < 9)
            {
                continue; //automatically can't be pandigital
            }
            else if (temp_a.size() + temp_b.size() + temp_prod.size() > 9)
            {
                break; //the product will only continue to get larger
            }
            else
            {
                if (set_bit_vec(temp_a, digits_seen) 
                    && set_bit_vec(temp_b, digits_seen) 
                    && set_bit_vec(temp_prod, digits_seen)
                    && !seen_prods[prod]) //if all digits are unique
                    //and the product has not already been seen
                {
                    //cout << a << "*" << b << "=" << prod << endl;
                    total += prod;
                    seen_prods[prod] = true;
                }
            } 
        }
    }

    cout << "total: " << total << endl;
    return 0;
}
