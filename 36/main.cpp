#include <iostream>
#include <string>
using namespace std;
//brute force
string get_binary(int n)
{
    string result;
    while (n > 0)
    {
        result = (char)((n % 2) + '0') + result;
        n /= 2;
    }
    return result;  
}

bool is_palindrome(string x)
{
    for (unsigned int i = 0; i < x.size() / 2; ++i)
    {
        if (x[i] != x[x.size() - 1 - i])
        {
            return false;
        }
    }
    return true;
}

int main()
{
    int total = 0;
    for (int i = 0; i < 1000000; ++i)
    {
        string num = to_string(i);
        if (is_palindrome(num))
        {
            string binary = get_binary(i);
            if (is_palindrome(binary))
            {
                cout << i << ":" << binary << endl;
                total += i;
            }
        }
    }
    cout << total << endl;
    return EXIT_SUCCESS;
}
