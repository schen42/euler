#include <iostream>
using namespace std;
//brute force
int main()
{
    int p_h = 0;
    int count_h = 0;
    for (int p = 1; p <= 1000; ++p)
    {
        int count = 0;
        for (int a = 1; a <= p - 1; ++a)
        {
            for (int b = 1; b <= a; ++b)
            {
                int c = p - a - b;
                if (c > 0 && a * a + b * b == c * c)
                {
                    ++count;
                }
            }
        }
        if (count > count_h)
        {
            p_h = p;
            count_h = count;
        }
    }
    cout << "p=" << p_h << " produces " << count_h << " solutions." << endl;
    return EXIT_SUCCESS;
}
