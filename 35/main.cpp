#include <iostream>
#include <string>
#define LOWER_LIMIT 2
#define UPPER_LIMIT 1000000
using namespace std;

/*
    Check if a number is prime, while also storing the answer in a bit vector
*/
bool is_prime(unsigned int x, bool known_primes[UPPER_LIMIT])
{
    if (x < 0 || x == 0 || x == 1) return false;
    if (known_primes[x]) return true;
    unsigned int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (unsigned int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    known_primes[x] = true;
    return true;
}

int main()
{
    bool known_primes[UPPER_LIMIT];
    for (int i = 0; i < UPPER_LIMIT; ++i)
        known_primes[i] = false;

    int total = 0; //number of primes less than 100
    for (unsigned int i = LOWER_LIMIT; i <= UPPER_LIMIT; ++i)
    {
        if (known_primes[i] || is_prime(i, known_primes)) //short-circuit eval
        {
            bool non_prime_found = false;
            string num = to_string(i);
            for (unsigned int rot = 1; rot < num.size(); ++rot)
            {
                string rotated = (num + num).substr(rot, num.size());
                if (!is_prime(atoi(rotated.c_str()), known_primes)) non_prime_found = true;
            }
            if (!non_prime_found)
            {
                //cout << i << endl;
                ++total;
            }
        }
    }
    cout << "total:" << total << endl;
    return EXIT_SUCCESS;
}
