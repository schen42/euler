#include <iostream>
#include <unordered_map>
#include <cmath>
using namespace std;

/*
    I disagree with most forum solutions.  Most of them generate an arbitrarily large number
    of primes, which I consider to be cheating, since once the answer is known, it is easy to
    increase performance simply by decreasing the upper bound on primes generated.  My algorithm
    makes no assumptions as to how large a prime has to be, and continues adds primes as the
    solution is searched (until the data type is too small to hold the value).
*/

bool is_prime(int x)
{
    static unordered_map<int, bool> primes({{2, true}, {3, true}, {5, true}, {7, true}});
    if (primes[x] == true) return true;
    if (x < 0 || x == 0 || x == 1) return false;
    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    primes[x] = true;
    return true;
}

int get_nth_prime(int n)
{
    static unordered_map<int, int> primes({{0, 2}, {1, 3}, {2, 5}, {3, 7}});
    static int highest_prime_found = 3;
    if (n > highest_prime_found)
    {
        for (int i = primes[highest_prime_found] + 1; ; ++i)
        {
            if (is_prime(i))
            {
                ++highest_prime_found;
                primes[highest_prime_found] = i;
                if (highest_prime_found == n)
                {
                    return i;
                }
            }
        }
    }
    else
    {
        return primes[n];
    }
}

int get_nth_square(int n)
{
    static unordered_map<int, int> squares({{0, 0}, {1, 1}, {2, 4}, {3, 9}});
    static int highest_square_found = 3;
    if (n > highest_square_found)
    {
        //populate the map until we have computed the square
        for (int i = highest_square_found + 1; i <= n; ++i)
        {
            squares[i] = i * i;
        }
        return squares[n];
    }
    return squares[n];
}

int main()
{
    //loop through all odd numbers
    //start at 33
    for (int i = 33; ; i += 2)
    {
        if (!is_prime(i)) //only check the composite numbers
        {
            bool is_goldbach = false;
            //for every prime number that can fit up to i - 2 (since 2 * 1**2 is the smallest addend)
            for (int j = 3; get_nth_prime(j) <= i - 2 && !is_goldbach; ++j)
            {
                for (int z = 1; get_nth_prime(j) + 2 * get_nth_square(z) <= i; ++z)
                {
                    if (get_nth_prime(j) + 2 * get_nth_square(z) == i)
                    {
                        is_goldbach = true;
                        //cout << i << " = " << get_nth_prime(j) << " + 2*" << sqrt(get_nth_square(z)) << "^2" << endl; 
                        break;
                    }
                }
            }
            if (!is_goldbach)
            {
                cout << i << endl;
                return EXIT_SUCCESS;
            }
        }
        else
        {
            continue;
        }
    }    
    return EXIT_FAILURE;
}