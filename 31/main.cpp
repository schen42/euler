#include <iostream>
#include <set>
#include <vector>
#include <iomanip>
using namespace std;
#define NUMBER_OF_COINS 8
#define LIMIT 200

/* The C++ solution doesn't work, I think, because of double precision issues.
    Notice in the print out that duplicate doubles appear in the set.  Saved for posterity */
int main()
{
    //1p, 2p, 5p, 10p, 20p, 50p, 100p and 200p.
    int primes[NUMBER_OF_COINS] =      {2, 3, 5, 7,  11, 13, 17,  19};
    int coin_values[NUMBER_OF_COINS] = {1, 2, 5, 10, 20, 50, 100, 200};

    //an array of sets that contain "hashes" for each unique combination for a particular combination amount
    //the hash is a product of primes, with each prime representing a coin
    set<double> *combos = new set<double>[LIMIT + 1];
    combos[0].insert(1);
    int prev_index;
    for (int i = 1; i < LIMIT + 1; ++i)
    {
        for (int j = 0; j < NUMBER_OF_COINS; ++j) //for each coin
        {
            prev_index = i - coin_values[j];
            if (prev_index >= 0)
            {
                //for each combo, make a new combo by adding the coin to the set
                for (auto combo = combos[prev_index].begin(); combo != combos[prev_index].end(); ++combo)
                {
                    combos[i].insert((*combo) * primes[j]);
                }
            }
            else
            {
                break;
            }
        }
    }

    if (LIMIT != 200)
    {
        for (int i = 1; i < LIMIT + 1; ++i)
        {
            cout << i << " (" << combos[i].size() << "): ";
            for (auto itr = combos[i].begin(); itr != combos[i].end(); ++itr)
            {
                cout << *itr << " ";
            }
            cout << endl;
        }
    }
    else
    {
        cout << "200 has " << combos[200].size() << endl;
    }
    

    delete [] combos;
    return 0;
}