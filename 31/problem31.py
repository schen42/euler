# Using python because double precision is not enough in C++ and I'm tired
# of using my crappy custom library / gmp

def main():
    #1p, 2p, 5p, 10p, 20p, 50p, 100p and 200p.

    primes = [2, 3, 5, 7,  11, 13, 17,  19]
    coin_values = [1, 2, 5, 10, 20, 50, 100, 200]


    #an array of sets that contain "hashes" for each unique combination for a particular combination amount

    #the hash is a product of primes, with each prime representing a coin

    combos = [set() for index in xrange(201)]
    combos[0].add(1)
    prev_index = 0;
    for i in range(0, 201):
        for j in range(0, 8): # for each coin
            prev_index = i - coin_values[j];
            if (prev_index >= 0):
                #for each combo, make a new combo by adding the coin to the set
                for combo in combos[prev_index]:
                    combos[i].add(combo * primes[j]);
            else:
                break


    print "2 pounds can be made in " + str(len(combos[200])) + " ways."

main()
