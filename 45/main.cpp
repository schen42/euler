#include <iostream>
#include <unordered_set>
using namespace std;

uint get_tri_val(uint n)
{
    return n * (n + 1) / 2;
}

uint get_pent_val(uint n)
{
    return n * (3 * n - 1) / 2;
}

uint get_hex_val(uint n)
{
    return n * (2 * n - 1);
}

bool in_set(const unordered_set<uint> &s, const uint &val)
{
    return s.count(val) != 0;
}

/*
    This algorithm updates all sets of numbers in a way that keeps them at similar values
    The reason for this is so that the values all grow at the same time so we search only
    enough values and no more.

    The order in which values are updated are: hexagonal, triangular, pentagonal.  Since we start
    at indices and values equal to zero, the first value to update is hexagonal, which grows at
    the greatest rate.  Then, triangular will update and finally pentagonal.

    Therefore, we check to see if all numbers have been encountered while updating the pentagonal
    numbers.  Therefore, we only need to check when looping through one set, and the other two
    hash sets indicating if a particular number has been encountered are guaranteed to have been populated.

    Runtime on my VM is approximately 80ms.

    We can go even faster by starting at the known indices that produce value 40755, but only improves by 
    approximately 10ms at best.

    Final improvement: Instead of using hash tables to check for the same value, just add the equivalence
    check in the pentagonal numbers loop.  Final run-time is approximately 8ms.
*/

int main()
{
    //unordered_set<uint> triangular;
    //unordered_set<uint> hexagonal;
    uint last_tri_i = 285; uint last_tri_val = 40755;
    uint last_pent_i = 165; uint last_pent_val = 40755;
    uint last_hex_i = 143; uint last_hex_val = 40755;
    while (true)
    {
        while (last_tri_val < last_hex_val)
        {
            last_tri_val = get_tri_val(last_tri_i);
            ++last_tri_i;
            //triangular.insert(last_tri_val);
        }
        while (last_pent_val < last_hex_val)
        {
            last_pent_val = get_pent_val(last_pent_i);
            ++last_pent_i;
            //if (in_set(triangular, last_pent_val) && in_set(hexagonal, last_pent_val))
            if (last_tri_val == last_pent_val && last_pent_val == last_hex_val)
            {
                cout << last_pent_val << endl;
                return EXIT_SUCCESS;
            }
        }
        ++last_hex_i;
        last_hex_val = get_hex_val(last_hex_i);
        //hexagonal.insert(last_hex_val);
    }
    return EXIT_FAILURE;
}