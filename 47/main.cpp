#include <iostream>
#include <unordered_map>
using namespace std;

//Hashing primes is more costly than just checking primes on my machine for this
//particular problem
bool is_prime(int x)
{
    if (x < 0 || x == 0 || x == 1) return false;
    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    return true;
}

int num_distinct_factors(int n, int primes[])
{
    int curr_prime_index = 0; //0th prime
    int num_distinct = 0;
    bool curr_factor_used = false;
    while (n > 1)
    {
        int curr_factor = primes[curr_prime_index];
        if (n % curr_factor == 0)
        {
            n /= curr_factor;
            if (!curr_factor_used)
            {
                ++num_distinct;
            }
            curr_factor_used = true;
        }
        else
        {
            if (is_prime(n))
            {
                return num_distinct + 1;
            }
            ++curr_prime_index;
            curr_factor_used = false;
        }
    }
    return num_distinct;
}

int main()
{
    /*
        the 140,000th is approximately one of the larger primes that can fit into an into
        pre-populate primes into stack for faster retrieval (approximately half a megabyte)
        this reduces runtime significantly on my machine
        relaxing this requirement significantly increases performance, but I consider this to be
        cheating
    */
    int primes[140000];
    int primes_found = 0;
    for (int i = 2; primes_found < 140000; ++i)
    {
        if (is_prime(i))
        {
            primes[primes_found] = i;
            ++primes_found;   
        }
    }

    int curr_num = 646; //start at 646
    int d1(0), d2(0), d3(0); //previous, 2 previous, 3 previous, respectively (values)
    int n1(0), n2(0), n3(0); //number of factors
    while (true)
    {
        int curr_distinct = num_distinct_factors(curr_num, primes);
        if (curr_distinct == 4 && n1 == 4 && n2 == 4 && n3 == 4)
        {
            cout << curr_num << "," << d1 << "," << d2 << "," << d3 << endl;
            return EXIT_SUCCESS;
        }
        d3 = d2;
        d2 = d1; 
        d1 = curr_num;
        n3 = n2;
        n2 = n1;
        n1 = curr_distinct;
        ++curr_num;
    }
    return EXIT_FAILURE;
}