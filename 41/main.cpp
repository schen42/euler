#include <iostream>
using namespace std;
typedef unsigned long long lint;

bool is_prime(int x)
{   
    if (x < 0 || x == 0 || x == 1) return false;
    
    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    return true;
}

/*
    Copy the bit vector that indicates if a digit has been used when generating the pandigital numbers
    into another bit vector.  Also, set an arbitrary index equal to true, if specified (shorthand for copying 
    and setting one value)
*/
void copy_bit_vec(bool a[], bool b[], int num_to_copy, int index_to_init_true = -1)
{
    for (int i = 0; i < num_to_copy; ++i)
    {
        b[i] = a[i];
    }
    if (index_to_init_true != -1)
        b[index_to_init_true] = true;
}

/*
    Initialize a bit vector of arbitrary size to false, except the first (zero-th) element.  The purpose
    of this is to disqualify 0 as a digit that can be used in a pandigital number and to make 
    indexing into this bit vector easy (i.e. if I want to see if digit 9 has been used, I just need to 
    use the index 9 instead of 8)
*/
void init_bit_vec(bool a[], int num_to_init)
{
    a[0] = true;
    for (int i = 1; i < num_to_init; ++i)
    {
        a[i] = false;
    }
}

/*
    Recursively generate pandigital numbers (so we only need to check the primality of 9*9! numbers instead of 
    987 million) of a certain length.

    @param root: The current number computed
    @param used_digits: Bit vector representing used digits
    @param length of the pandigital (i.e. length = 4 for 1234)
    @param level: The depth of the recursion, used to find powers of 10
    @param largest_num: A variable which is set to the largest pandigital prime found.
*/
void generate_pandigital(lint root, bool used_digits[], uint length, uint level, lint &largest_num)
{
    static lint powers_10[10] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
    if (level == length && is_prime(root))
    {
        if (root > largest_num)
            largest_num = root;
        return;
    }

    for (uint i = 1; i < length + 1; ++i)
    //for (uint i = length; i >= 1; --i)
    {
        if (!used_digits[i])
        {
            bool temp_used[length + 1];
            copy_bit_vec(used_digits, temp_used, length + 1, (uint)i);
            generate_pandigital(i * powers_10[level] + root, temp_used, length, level + 1, largest_num);
        }
    }
}

int main()
{
    //generate 1-n pandigitals, starting with the large numbers first
    //That way, once we find a pandigital prime, we know it is the largest
    for (int n = 9; n >= 4; --n) //4 since we know a 1 to 4 pandigital
    {
        lint largest_num = 0;
        bool used_digits[n + 1];
        init_bit_vec(used_digits, n + 1);
        generate_pandigital(0, used_digits, n, 0, largest_num);
        if (largest_num != 0)
        {
            cout << "largest pandigital prime is " << largest_num << endl;
            break;
        }
    }
    return EXIT_SUCCESS;
}

//reference 32, 38 for other pandigital problem