Contains problems I solved after 12/2013 (which is when I started saving my code)

Performance measured on a i7-2360QM with 6GB of RAM on an ubuntu virtual machine
with 1 uncapped CPU and 1.5GB of RAM.  Compiled in G++ 4.6.3.
 
    g++ main.cpp -g -std=c++0x -Wall -o prog
