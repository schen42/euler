#include <iostream>
#include <string.h>
using namespace std;

bool is_prime(int x)
{
    if (x < 0 || x == 0 || x == 1) return false;
    int step = 2 - (x % 2); //step size 2 if number is even (slight optimization)
    for (int i = 2; i * i <= x; i += step)
    {
        if (x % i == 0) //divisible by that is not 1 or itself
            return false;
    }
    return true;
}

int main()
{
    bool bit_vector[10];
    for (int i = 1000; i <= 9999; ++i)
    {
        bool is_permutation = true;
        memset(bit_vector, 0, sizeof(bool) * 10); //sizeof(bool) = 2
        if (is_prime(i) && is_prime(i + 3330) && is_prime(i + 6660))
        {
            string temp = to_string(i);
            for (uint j = 0; j < temp.size(); ++j)
            {
                bit_vector[temp[j] - '0'] = true;
            }
            temp = to_string(i + 3330);
            for (uint j = 0; j < temp.size(); ++j)
            {
                if (bit_vector[temp[j] - '0'] == false)
                    is_permutation = false;
            }
            temp = to_string(i + 6660);
            for (uint j = 0; j < temp.size(); ++j)
            {
                if (bit_vector[temp[j] - '0'] == false)
                    is_permutation = false;
            }
            if (is_permutation)
            {
                cout << i << "," << i + 3330 << "," << i + 6660 << endl;
                cout << i << i + 3330 << i + 6660 << endl;
            }
        }
    }
    return EXIT_SUCCESS;
}