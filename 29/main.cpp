#include <iostream>
#include <unordered_map>
#include <map>
#include <math.h>
using namespace std;

//run-time approx 20 ms

int main()
{
    unordered_map<double, bool> generated_values;
    for (int a = 2; a <= 100; ++a)
    {
        for (int b = 2; b <= 100; ++b)
        {
            generated_values[pow(a, b)] = true;
        }
    }
    cout << "Count=" << generated_values.size() << endl;
    return 0;
}