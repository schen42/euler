#include <iostream>
#include <string>
#include <math.h>
using namespace std;
#define LIMIT 1000000

//~80 ms
void string_method()
{
    int digits_added = 0;
    string d = "";
    for (int n = 1; digits_added < LIMIT; ++n)
    {
        string num = to_string(n);
        d += num;
        digits_added += num.size();
    }
    cout << (d[0] - '0') * (d[9] - '0') * (d[99] - '0') * (d[999] - '0')
        * (d[9999] - '0') * (d[99999] - '0') * (d[999999] - '0') << endl;
}

int get_magnitude(int n)
{
    int r = 0;
    while (n > 0)
    {
        n /= 10;
        ++r;
    }
    return r;
}

//much slower than just concatenating strings, interestingly, probably because of division
void array_method()
{
    char irrational[LIMIT];
    int num = 1;
    int index = 0;
    while (index < LIMIT)
    {
        int temp = 0;
        for (int i = get_magnitude(num) - 1; i >= 0; --i, ++index)
        {
            irrational[index] = (num - temp) / (int)pow(10, i);
            temp = num / (int)pow(10, i) * (int)pow(10, i);
        }
        ++num;
    }
    char* d = irrational;
    cout << d[0] * d[9] * d[99] * d[999] * d[9999] * d[99999] * d[999999] << endl;
}

int main()
{
    string_method();
    //array_method();
    return EXIT_SUCCESS;
}
