#include <iostream>
#include <string>
using namespace std;

/*
    Return the number of digits added to the bit vector if the number is 
    possible pandigital.  Otherwise, return -1.
*/
bool is_possibly_pandigital(bool bit_vec[], const string &num)
{
    for (unsigned int i = 0; i < num.size(); ++i)
    {
        if (bit_vec[num[i] - '0'] == true)
            return false;
        bit_vec[num[i] - '0'] = true;
    }
    return true;
}

int main()
{
    //4 digit * 1 digit = 4 digits
    //4 digit * 2 digit = 4 digits minimum, 5 digits maximum
    //9999 decent upper bound, euler gives us 192 as one answer
    int largest_pandigital, lpn; //lpn n of the largest multiplicant
    for (int i = 9; i <= 9999; ++i)
    {
        bool seen_digits[10] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        string num = to_string(i);
        int num_digits = 0;
        bool is_pandigital = true;
        int n = 2;
        while (num_digits < 9)
        {
            is_pandigital &= is_possibly_pandigital(seen_digits, num);
            num_digits += num.size();
            num = to_string(i * n);
            ++n;
        }

        if (is_pandigital && num_digits == 9)
        {
            largest_pandigital = i;
            lpn = n - 2;
        }
    }

    //the largest multiplicand found must form the largest concatenated pandigital number since
    //the first value (m * 1) comprises the most significant digits of the concatenated number
    cout << "answer: ";
    for (int i = 1; i < lpn + 1; ++i)
    {
        cout << largest_pandigital * i;
    }
    cout << endl;
    return EXIT_SUCCESS;
}

//reference 32 for other pandigital problem