#include <iostream>
#include <cstdlib>
#include "CrappyBigInt.h"

//Run time ~200 ms

using namespace std;
    
int main()
{
    /*
    mpz_t f0;
    mpz_t f1;
    mpz_t next_fib;
    mpz_t limit;
    mpz_inits(f0, f1, next_fib, NULL);
    mpz_set(f0, 1);
    mpz_set(f1, 1);
    mpz_set(limit)
    */

    BigInt f0("1");
    BigInt f1("1");
    BigInt fib;
    BigInt temp;
    int count = 2;
    while (fib.get_value().size() < 1000)
    {
        fib = f0 + f1;
        f0 = BigInt(f1.get_value());
        f1 = BigInt(fib.get_value());
        ++count;

        //cout << fib.get_value() << endl;
    }

    cout << count << ":" << fib.get_value() << endl;
    
    return 0;
}