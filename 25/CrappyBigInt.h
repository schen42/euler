#ifndef _CRAPPY_BIG_INT_
#define _CRAPPY_BIG_INT_
#include <iostream>
#include <string>
#include <math.h>
#include <cassert>
/*
    Several assumptions: 
    -No negative numbers
    -Strings only contain digits
*/

class BigInt
{
private:
    std::string value;
public:
    BigInt()
    {
        value = "0";
    }

    BigInt(std::string val)
    {
        value = val;
    }

    std::string get_value() const
    {
        return this->value;
    }

    BigInt operator+(BigInt &b)
    {
        std::string temp_a(this->value);
        std::string temp_b(b.get_value());
        //First, determine longer string, so we can pad it
        int diff = abs(temp_a.size() - temp_b.size());
        if (temp_a.size() < temp_b.size())
        {
            temp_a = std::string(diff, '0') + temp_a;
        }
        else if (temp_a.size() > temp_b.size())
        {
            temp_b = std::string(diff, '0') + temp_b;
        }

        //std::cout << temp_a << "+" << temp_b << std::endl;

        int carry = 0;
        int digit = 0;
        int d0, d1, sum;
        std::string to_return = "";
        for (int i = temp_a.size() - 1; i >= 0; --i)
        {
            d0 = temp_a[i] - '0';
            d1 = temp_b[i] - '0';
            sum = d0 + d1 + carry;
            if (sum >= 10)
            {
                digit = sum - 10;
                carry = 1;
            }
            else
            {
                digit = sum;
                carry = 0;
            }
            to_return.insert(to_return.begin(), '0' + digit);
        }
        if (carry == 1) to_return.insert(to_return.begin(), '1');
        return BigInt(to_return);
    }
};

#endif
